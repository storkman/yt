#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "lua/src/lua.h"
#include "lua/src/lualib.h"
#include "lua/src/lauxlib.h"
#include <curl/curl.h>

#include "common.h"
#include "json.h"

#define RBUFLEN (4*1024*1024)

struct req_state {
	struct req_state *next;
	CURL *handle;
	char *url;
	struct curl_slist *headers;
	int p;
	int completed;
	
	char error[CURL_ERROR_SIZE];
	CURLcode status;
	char buf[RBUFLEN];
};

static int	url_cookies(lua_State*);
static int	url_get(lua_State*);
void	url_init(lua_State*);
static int	url_wait(lua_State*);

static void	_req_reset(struct req_state*);
static void	_req_start(lua_State*, struct req_state*);
static int	_req_wait_any(lua_State*);
static int	req_gc(lua_State*);
static int	req_complete(lua_State*);
static int	req_result(lua_State*);
static int	req_start(lua_State*);
static int	req_wait(lua_State*);

static struct luaL_Reg url_lib[] = {
	{ "get",	url_get },
	{ "setCookies",	url_cookies },
	{ "wait",	url_wait },
	{ NULL,	NULL }
};

const char *req_type = "url-request";
static struct luaL_Reg req_meta[] = {
	{ "__gc",	req_gc },
	{ "onComplete",	req_complete },
	{ "result",	req_result },
	{ "start",	req_start },
	{ "wait",	req_wait },
	{ NULL,	NULL },
};

static int maxconn = 24;
static const char *cookies;
static CURLM *curl;

void
url_init(lua_State *L)
{
	curl = curl_multi_init();
	if (!curl)
		die("failed to create libcurl handle");
	curl_multi_setopt(curl, CURLMOPT_MAX_TOTAL_CONNECTIONS, maxconn);
	luaL_newmetatable(L, req_type);
	luaL_setfuncs(L, req_meta, 0);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	lua_pop(L, 1);
	
	luaL_newlib(L, url_lib);
	lua_setglobal(L, "url");
}

int
url_cookies(lua_State *L)
{
	struct stat st;
	if (lua_isnil(L, 1)) {
		cookies = NULL;
		return 0;
	}
	cookies = luaL_checkstring(L, 1);
	if (stat(cookies, &st))
		return luaL_error(L, "%s: cookies not accessible", cookies);
	return 0;
}

static size_t
write_callback(char *ptr, size_t size, size_t nm, void *ud)
{
	struct req_state *req = ud;
	int nb = sizeof(req->buf) - req->p;
	if (nm < nb)
		nb = nm;
	memmove(req->buf + req->p, ptr, nb);
	req->p += nb;
	return nb;
}

int
url_get(lua_State *L)
{
	struct req_state *dst;
	const char *url, *hd;
	
	url = luaL_checkstring(L, 1);
	luaL_checktype(L, 2, LUA_TTABLE);
	
	dst = lua_newuserdata(L, sizeof(*dst));
	memset(dst, 0, sizeof(*dst));
	luaL_setmetatable(L, req_type);
	dst->url = strdup(url);
	dst->handle = curl_easy_init();
	
	lua_pushnil(L);
	while (lua_next(L, 2)) {
		hd = lua_tostring(L, -1);
		if (!hd)
			return luaL_error(L, "non-string element in request header table");
		dst->headers = curl_slist_append(dst->headers, hd);
		lua_pop(L, 1);
	}
	_req_reset(dst);
	return 1;
}

int
url_wait(lua_State *L)
{
	struct req_state *req;
	if (!_req_wait_any(L))
		return 0;
	req = lua_touserdata(L, -1);
	if (req->error[0])
		lua_pushstring(L, req->error);
	else
		lua_pushnil(L);
	return 2;
}

int
req_gc(lua_State *L)
{
	struct req_state *req = luaL_checkudata(L, 1, req_type);
	curl_easy_cleanup(req->handle);
	curl_slist_free_all(req->headers);
	return 0;
}

int
req_complete(lua_State *L)
{
	luaL_checkudata(L, 1, req_type);
	luaL_checktype(L, 2, LUA_TFUNCTION);
	lua_setuservalue(L, 1);
	return 1;
}

void
_req_reset(struct req_state *req)
{
	CURL *handle = req->handle;
	curl_easy_setopt(handle, CURLOPT_COOKIEFILE, cookies);
	curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, req->error);
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, req);
	curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(handle, CURLOPT_ACCEPT_ENCODING, "");
	
	curl_easy_setopt(handle, CURLOPT_URL, req->url);
	curl_easy_setopt(handle, CURLOPT_HTTPHEADER, req->headers);
}

int
req_result(lua_State *L)
{
	struct req_state *req = luaL_checkudata(L, 1, req_type);
	lua_pushlstring(L, req->buf, req->p);
	if (req->error[0])
		lua_pushstring(L, req->error);
	else
		lua_pushnil(L);
	return 2;
}

void
_req_start(lua_State *L, struct req_state *req)
{
	CURLMcode err = curl_multi_add_handle(curl, req->handle);
	if (err)
		luaL_error(L, "failed to start request: %s", curl_multi_strerror(err));
	
	lua_pushlightuserdata(L, req->handle);
	lua_pushvalue(L, -2);
	lua_rawset(L, LUA_REGISTRYINDEX);
}	

int
req_start(lua_State *L)
{
	struct req_state *req = luaL_checkudata(L, 1, req_type);
	_req_start(L, req);
	return 1;
}

int
req_wait(lua_State *L)
{
	struct req_state *req = luaL_checkudata(L, 1, req_type);
	while (!req->completed) {
		if (_req_wait_any(L))
			lua_pop(L, 1);
		else
			return luaL_error(L, "request.wait: no active connections");
	}
	return req_result(L);
}

static int
_wait_req(lua_State *L)
{
	int n;
	struct CURLMsg *msg = NULL;
	struct req_state *req = NULL;
	int nconn, type;
	CURLMcode err = curl_multi_perform(curl, &nconn);
	if (err)
		luaL_error(L, "transfer error: %s", curl_multi_strerror(err));
	for (;;) {
		msg = curl_multi_info_read(curl, &n);
		if (msg)
			break;
		if (!nconn)
			return 0;
		err = curl_multi_perform(curl, &nconn);
		if (err)
			luaL_error(L, "transfer error: %s", curl_multi_strerror(err));
	}
	lua_pushlightuserdata(L, msg->easy_handle);
	lua_gettable(L, LUA_REGISTRYINDEX);
	req = lua_touserdata(L, -1);
	
	req->completed = 1;
	req->status = msg->data.result;
	curl_multi_remove_handle(curl, msg->easy_handle);
	
	lua_pushlightuserdata(L, msg->easy_handle);
	lua_pushnil(L);
	lua_settable(L, LUA_REGISTRYINDEX);
	
	type = lua_getuservalue(L, -1);
	if (type != LUA_TFUNCTION) {
		lua_pop(L, 1);
		return 1;
	}
	lua_pushvalue(L, -2);
	if (lua_pcall(L, 1, 1, 0) != LUA_OK) {
		fprintf(stderr, "%s\n", lua_tostring(L, -1));
		lua_pop(L, 1);
		return 1;
	}
	if (!lua_isnil(L, -1)) {
		lua_pop(L, 1);
		_req_reset(req);
		_req_start(L, req);
		lua_pop(L, 1);
		return -1;
	}
	lua_pop(L, 1);
	return 1;
}

int
_req_wait_any(lua_State *L)
{
	int n;
	do {
		n = _wait_req(L);
	} while (n < 0);
	return n;
}
