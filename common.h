/* #include "lua/src/lua.h" */

void die(const char *fmt, ...);
void loadembed(lua_State *L, const char *name, const char *start, const char *end);
void runembed(lua_State *L, const char *name, const char *start, const char *end);
int readall(char *buf, int bs, int fd);
int spawn(char *const *arg, int *fd);
int spawn1(const char *prog, const char *arg);
