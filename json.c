#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "lua/src/lua.h"
#include "lua/src/lualib.h"
#include "lua/src/lauxlib.h"

#include "libutf/utf.h"
#include "common.h"
#include "json.h"

struct buff {
	const char *data;
	int len;
	int p;
	
	const char *error;
};

static int next(struct buff *buf, char c);
static int nextword(struct buff *buf);
static int perr(struct buff *buf, const char *err);
static void token(struct buff *buf);

static int parseany(lua_State *L, struct buff *buf, int depth);
static int parsearr(lua_State *L, struct buff *buf, int depth);
static int parsenum(lua_State *L, struct buff *buf);
static int parseobj(lua_State *L, struct buff *buf, int depth);
static int parsestr(lua_State *L, struct buff *buf);
static void pushobj(lua_State *L, struct buff *buf, const char *t);

const char*
json_parse(lua_State *L, const char *data, int len)
{
	struct buff buf;
	buf.data = data;
	buf.len = len;
	buf.p = 0;
	buf.error = NULL;
	parseany(L, &buf, 0);
	return buf.error;
}

int
lua_json_parse(lua_State *L)
{
	struct buff buf;
	size_t len;
	const char *data = luaL_checklstring(L, 1, &len);
	buf.data = data;
	buf.len = len;
	buf.p = 0;
	if (parseany(L, &buf, 0))
		return luaL_error(L, "parse error: %s", buf.error);
	return 1;
}

static struct luaL_Reg ljglobals[] = {
	{ "parse",	lua_json_parse },
	{ NULL,	NULL },
};

int
luaopen_json(lua_State *L)
{
	luaL_newlibtable(L, ljglobals);
	luaL_setfuncs(L, ljglobals, 0);
	lua_pushvalue(L, -1);
	lua_setglobal(L, "json");
	return 1;
}

/* next(c) - return true and advance position if peek() == c */
static int
next(struct buff *buf, char c)
{
	if (buf->p >= buf->len)
		return 0;
	if (buf->data[buf->p] != c)
		return 0;
	buf->p++;
	return 1;
}

static int
nextany(struct buff *buf, const char *set)
{
	while (*set) {
		if (buf->data[buf->p] == *set)
			return 1;
		set++;
	}
	return 0;
}

static int
perr(struct buff *buf, const char *err)
{
	fputs(err, stderr);
	return 1;
}

/* token - drop spaces */
static void
token(struct buff *buf)
{
	while (buf->p < buf->len) {
		switch (buf->data[buf->p]) {
		case ' ':	case '\t':
		case '\n':	case '\r':
			buf->p++;
			break;
		default:
			return;
		}
	}
}

/* nextword - peek until any non-alphanumeric or EOF. Return length. */
static int
nextword(struct buff *buf)
{
	int n = 0;
	while (n < 6 && buf->p + n < buf->len) {
		if (!isalnum(buf->data[buf->p + n]))
			break;
		n++;
	}
	return n;
}

/* completely arbitrary */
#define MAXLEN 4096
#define MAXDEPTH 1024

static int
parseany(lua_State *L, struct buff *buf, int depth)
{
	int n;
	if (depth > MAXDEPTH)
		return perr(buf, "exceeded maximum depth");
	if (!lua_checkstack(L, 3))
		return perr(buf, "can't allocate enough Lua stack space");
	token(buf);
	if (next(buf, '"'))
		return parsestr(L, buf);
	if (nextany(buf, "-1234567890"))
		return parsenum(L, buf);
	
	if (next(buf, '{'))
		return parseobj(L, buf, depth+1);
	if (next(buf, '['))
		return parsearr(L, buf, depth+1);
	
	n = nextword(buf);
	if (strncmp(buf->data + buf->p, "true", n) == 0) {
		lua_pushboolean(L, 1);
		buf->p += n;
		return 0;
	}
	if (strncmp(buf->data + buf->p, "false", n) == 0) {
		lua_pushboolean(L, 0);
		buf->p += n;
		return 0;
	}
	if (strncmp(buf->data + buf->p, "null", n) == 0) {
		lua_pushnil(L);
		buf->p += n;
		return 0;
	}
	return perr(buf, "unrecognized token");
}

static int
parsearr(lua_State *L, struct buff *buf, int depth)
{
	int i;
	token(buf);
	pushobj(L, buf, "array");
	if (next(buf, ']'))
		return 0;
	
	for (i = 1; ; i++) {
		if (i > MAXLEN)
			return perr(buf, "exceeded maximum array length");
		token(buf);
		lua_pushinteger(L, i);
		if (parseany(L, buf, depth+1))
			return 1;
		lua_settable(L, -3);
		
		token(buf);
		if (next(buf, ']'))
			break;
		if (!next(buf, ','))
			return perr(buf, "expected ',' or ']' after array element");
	}
	return 0;
}

static int
parseobj(lua_State *L, struct buff *buf, int depth)
{
	token(buf);
	pushobj(L, buf, "object");
	if (next(buf, '}'))
		return 0;
	
	for (;;) {
		token(buf);
		if (!next(buf, '"'))
			return perr(buf, "object key must be a string");
		if (parsestr(L, buf))
			return 1;
		
		token(buf);
		if (!next(buf, ':'))
			return perr(buf, "expected ':' after object key");
		if (parseany(L, buf, depth+1))
			return 1;
		lua_settable(L, -3);
		
		token(buf);
		if (next(buf, '}'))
			return 0;
		if (!next(buf, ','))
			return perr(buf, "expected ',' or '}' after object element");
	}
}

static void
pushobj(lua_State *L, struct buff *buf, const char *t)
{
	/* setmetatable({}, {JSONType = t}) */
	lua_newtable(L);
	lua_createtable(L, 0, 2);
	
	lua_pushliteral(L, "JSONType");
	lua_pushstring(L, t);
	lua_settable(L, -3);
	
	lua_setmetatable(L, -2);
}

#define MAXSTR (32*1024)

static int
parsestr(lua_State *L, struct buff *buf)
{
	int sl = 0, i;
	char c;
	Rune sc;
	char ub[5];
	char str[MAXSTR];
	while (buf->p < buf->len) {
		if (sl >= MAXSTR)
			return perr(buf, "string too long");
		c = buf->data[buf->p];
		if (c == '"') {
			lua_pushlstring(L, str, sl);
			buf->p++;
			return 0;
		}
		if (c == '\\') {
			buf->p++;
			if (buf->p >= buf->len)
				return perr(buf, "end of input in string escape");
			c = buf->data[buf->p];
			switch (c) {
			case '"':
			case '\\':
			case '/':
				sc = c;
				break;
			case 'b':
				sc = '\b';
				break;
			case 'f':
				sc = '\f';
				break;
			case 'n':
				sc = '\n';
				break;
			case 'r':
				sc = '\r';
				break;
			case 't':
				sc = '\t';
				break;
			case 'u':
				buf->p++;
				if (buf->p + 4 > buf->len)
					return perr(buf, "truncated unicode escape");
				for (i = 0; i < 4; i++) {
					c = buf->data[buf->p + i];
					if ((c < '0' || c > '9') && (c < 'a' || c > 'f') && (c < 'A' || c > 'F'))
						return perr(buf, "invalid unicode escape");
					ub[i] = c;
				}
				ub[4] = 0;
				sc = strtol(ub, NULL, 16);
				buf->p += 3;
				break;
			default:
				return perr(buf, "invalid escape");
			}
			sl += runetochar(str + sl, &sc);
			buf->p++;
			continue;
		}
		str[sl] = c;
		sl++;
		buf->p++;
	}
	return perr(buf, "unterminated string");
}

static void
skipnum(struct buff *buf)
{
	while (buf->data[buf->p] >= '0' && buf->data[buf->p] <= '9')
		buf->p++;
}

static int
parsenum(lua_State *L, struct buff *buf)
{
	int s = buf->p;
	char st[MAXSTR];
	next(buf, '-');
	skipnum(buf);
	if (next(buf, '.'))
		skipnum(buf);
	if (nextany(buf, "eE")) {
		buf->p++;
		if (nextany(buf, "+-"))
			buf->p++;
		skipnum(buf);
	}
	if (buf->p - s >= MAXSTR)
		return perr(buf, "number too long");
	memmove(st, buf->data + s, buf->p - s);
	st[buf->p - s] = 0;
	if (!lua_stringtonumber(L, st))
		return perr(buf, "can't convert number");
	return 0;
}
