# OAuthC

*oauthc* is an OAuth 2.0 client.
It handles obtaining, refreshing, and revoking access tokens.

     $ BROWSER=firefox oauthc -1 -id YOUR_ID -secret YOUR_SECRET -twitch -nochallenge -print    # interactively obtain authorization and print the token to stdout

## Dependencies

 - [curl](https://curl.haxx.se/) command line tool.
 - Any web browser that can handle the OAuth authorization process (*oauthc* will use `$BROWSER` by default, or `xdg-open` if not set).

## Options

- `-auth-ep` `URL`  Authentication endpoint.
- `-f` `path`  Authentication file path.
- `-id` `client_id`  Client ID string.
- `-nochallenge`  Do not use an RFC7636 code challenge. *Needed for Twitch*
- `-print`  Print the obtained authentication token on stdout.
- `-port` `N`  Use port number `N` instead of picking a random one. *Needed for Twitch*
- `-r`  Refresh only. Don't try to obtain a new authentication token interactively.
- `-revoke`  Revoke the authentication token.
- `-revoke-ep` `URL`  Revocation endpoint.
- `-scopes` `scope_urls`  Space-separated list of access scopes.
- `-secret` `client_secret`  Client secret string.
- `-token-ep` `URL`  Token endpoint.
- `-twitch`  Use the predefined endpoints for Twitch.
- `-yt`  Use the predefined endpoints for YouTube.
- `-1`  Obtain a new authentication token. Do not refresh.

## Authentication file

The authentication file consists of the following fields, on separate lines:

    access token (written by oauthc)
    refresh token (written by oauthc)
    auth endpoint URL
    token endpoint URL
    revoke endpoint URL (optional)
    scopes (space-separated)
    client ID
    client secret

If you're running *oauthc* for the first time, leave the access and refresh token fields empty.
