# See LICENSE file for copyright and license details.

PREFIX = /usr/local

UNICODE = 6.1.0

CFLAGS  = -ansi -pedantic -Os -Wall -Wextra
LDFLAGS = -s

CC  = cc
AWK = awk
