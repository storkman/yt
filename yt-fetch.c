#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "lua/src/lua.h"
#include "lua/src/lualib.h"
#include "lua/src/lauxlib.h"

#include "common.h"
#include "json.h"
#include "libs.h"

extern const char _binary_fetch_luac_start[];
extern const char _binary_fetch_luac_end[];

const char *argv0 = "yt-fetch";

int
main(int argc, char* argv[])
{
	const char *errstr;
	char **arg;
	lua_State *L = luaL_newstate();
	if (!L)
		die("can't initialize Lua: out of memory");
	
	luaL_openlibs(L);
	luaopen_json(L);
	yt_init(L);
	url_init(L);
	loadembed(L, "fetch.lua", _binary_fetch_luac_start, _binary_fetch_luac_end);
	
	for (arg = argv + 1; *arg && *arg[0] == '-'; arg++) {
		if (0) {
		} else if (strcmp(*arg, "-L") == 0) {
			lua_pushboolean(L, 1);
			lua_setglobal(L, "argLive");
		} else if (strcmp(*arg, "-t") == 0) {
			arg++;
			if (!*arg)
				die("no test URL given\n");
			lua_pushstring(L, *arg);
			lua_setglobal(L, "argTestURL");
		} else
			die("unknown option: %s\n", *arg);
	}

	switch (lua_pcall(L, 0, 0, 0)) {
	case LUA_ERRRUN:
		errstr = lua_tostring(L, -1);
		fprintf(stderr, "%s\n", errstr);
		die("Lua runtime error\n");
		break;
	case LUA_ERRMEM:
		die("internal error: Lua memory allocation error\n");
		break;
	case LUA_ERRERR:
		die("internal error: error while running Lua error handler\n");
		break;
	case LUA_ERRGCMM:
		die("internal error: error while running a __gc metamethod\n");
		break;
	}
}
